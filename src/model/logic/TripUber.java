package model.logic;

public class TripUber {

	
	public enum TipoTemporalidad{
		HORA,
		SEMANA,
		MES
	}
	
	private int sourceid;
	private int dstid;
	private int numeroTiempo;
	private double mean_travel_time;
	private double standard_deviation_travel_time;
	private double geometric_mean_travel_time;
	private double geometric_standard_deviation_travel_time;
	private TipoTemporalidad tipoTemp;
	
	
	
	public TripUber(int sourceid, int dstid,int hod, double mean_travel_time,
			double standard_deviation_travel_time, double geometric_mean_travel_time,
			double geometric_standard_deviation_travel_time,
			TipoTemporalidad tipoTemp) {
		
		
		this.sourceid = sourceid;
		this.dstid = dstid;
		this.numeroTiempo = hod;
		this.mean_travel_time = mean_travel_time;
		this.standard_deviation_travel_time = standard_deviation_travel_time;
		this.geometric_mean_travel_time = geometric_mean_travel_time;
		this.geometric_standard_deviation_travel_time = geometric_standard_deviation_travel_time;
		this.tipoTemp = tipoTemp;
	}



	public int getSourceid() {
		return sourceid;
	}



	public void setSourceid(int sourceid) {
		this.sourceid = sourceid;
	}



	public int getDstid() {
		return dstid;
	}



	public void setDstid(int dstid) {
		this.dstid = dstid;
	}



	public int getHod() {
		return numeroTiempo;
	}



	public void setHod(int hod) {
		this.numeroTiempo = hod;
	}



	public double getMean_travel_time() {
		return mean_travel_time;
	}



	public void setMean_travel_time(double mean_travel_time) {
		this.mean_travel_time = mean_travel_time;
	}



	public double getStandard_deviation_travel_time() {
		return standard_deviation_travel_time;
	}



	public void setStandard_deviation_travel_time(double standard_deviation_travel_time) {
		this.standard_deviation_travel_time = standard_deviation_travel_time;
	}



	public double getGeometric_mean_travel_time() {
		return geometric_mean_travel_time;
	}



	public void setGeometric_mean_travel_time(double geometric_mean_travel_time) {
		this.geometric_mean_travel_time = geometric_mean_travel_time;
	}



	public double getGeometric_standard_deviation_travel_time() {
		return geometric_standard_deviation_travel_time;
	}



	public void setGeometric_standard_deviation_travel_time(double geometric_standard_deviation_travel_time) {
		this.geometric_standard_deviation_travel_time = geometric_standard_deviation_travel_time;
	}



	public TipoTemporalidad getTipoTemp() {
		return tipoTemp;
	}



	public void setTipoTemp(TipoTemporalidad tipoTemp) {
		this.tipoTemp = tipoTemp;
	}
	
}
