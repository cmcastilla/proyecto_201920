package model.logic;

import java.io.File;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.opencsv.CSVReader;

import model.data_structures.ArregloDinamico;
import model.data_structures.IArregloDinamico;
import model.data_structures.LinkedStack;
import model.data_structures.ListaDinamica;
import model.logic.TripUber.TipoTemporalidad;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {

	/**
	 * Atributos del modelo del mundo
	 */
	private IArregloDinamico datos;
	
	
	private LinkedStack<TripUber> pilaViajes;
	private ListaDinamica<TripUber> listaDinamica;

	
	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		datos = new ArregloDinamico(7);
	}
	
	/**
	 * Constructor del modelo del mundo con capacidad dada
	 * @param tamano
	 */
	public MVCModelo(int trimestre)
	{
		cargar(trimestre);
	}
	
	public void cargar(int trimestre){
		
		try {
			pilaViajes = new LinkedStack<>();
			
			int contadorViajeSemana = 0;
			int contadorViajesMes = 0;
			int contadorViajesHora = 0;
			int menorZona = Integer.MAX_VALUE;
			int mayorZona = -1;
			
			File[] tripsFiles = new File("./data/archivosCargar").listFiles();
			for (int i = 0; i < tripsFiles.length; i++) {
				
				if(tripsFiles[i].getName().split("2018")[1].contains(""+trimestre))
				{
					 Reader reader = Files.newBufferedReader(Paths.get(tripsFiles[i].getPath()));
			            CSVReader csvReader = new CSVReader(reader);
			            
			            

			            
			            
			        
			            // Reading Records One by One in a String array
			            String[] nextRecord;
			            csvReader.readNext();
			            while ((nextRecord = csvReader.readNext()) != null) {
			            	
			            	
			            	
				            TipoTemporalidad temporalidad = null;
				            if(tripsFiles[i].getName().contains("Hourly"))
				            {
					            temporalidad = TripUber.TipoTemporalidad.HORA;
					            contadorViajesHora++;
					            

				            }
				            else if(tripsFiles[i].getName().contains("Monthly"))
				            {
					            temporalidad = TripUber.TipoTemporalidad.MES;
					            contadorViajesMes++;
				            }
				            else
				            {
					            temporalidad = TripUber.TipoTemporalidad.SEMANA;
					            contadorViajeSemana++;
				            }
				            				            
			            		            	
			            	int sourceid = Integer.parseInt(nextRecord[0]);
			            	 int dstid =  Integer.parseInt(nextRecord[1]);
			            	 int tiempo = Integer.parseInt(nextRecord[2]);
			            	 double mean_travel_time = Double.parseDouble(nextRecord[3]);
			            	 double standard_deviation_travel_time  = Double.parseDouble(nextRecord[4]);
			            	 double geometric_mean_travel_time  = Double.parseDouble(nextRecord[5]);
			            	 double geometric_standard_deviation_travel_time  = Double.parseDouble(nextRecord[6]);
			            	 
			            	 //menor
			            	 if(dstid < menorZona)
			            	 {
			            		 menorZona = dstid;
			            	 }
			            	 else if(sourceid < menorZona)
			            	 {
			            		 menorZona = sourceid;
			            	 }
			            	 
			            	 //mayor
			            	 if(dstid > mayorZona)
			            	 {
			            		 mayorZona = dstid;
			            	 }
			            	 else if(sourceid > mayorZona)
			            	 {
			            		 mayorZona = sourceid;
			            	 }
			            	 
			            	 TripUber viaje = new TripUber(sourceid, dstid, tiempo, mean_travel_time, standard_deviation_travel_time,
			            			 geometric_mean_travel_time, geometric_standard_deviation_travel_time, temporalidad);
			            	 
			            	 
			            	 listaDinamica.addLast(viaje);
			            	 
			            	 
			            	 
			            	
			            }
				}
				
				
	           
			}
			
			System.out.println("Total viajes hora : " + contadorViajesHora);
			System.out.println("Total viajes semana : " +  contadorViajeSemana);
			System.out.println("Total viajes mes : " + contadorViajesMes);
			System.out.println("La zona con mayor identificador es : "+ mayorZona);
			System.out.println("La zona con menor identificador es : "+ menorZona);


			
	        }
		catch(Exception e){
			e.printStackTrace();
			System.out.println("Fallo la carga");
		}
	
	}
	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int darTamano()
	{
		return datos.darTamano();
	}

	/**
	 * Requerimiento de agregar dato
	 * @param dato
	 */
	public void agregar(String dato)
	{	
		datos.agregar(dato);
	}
	
	/**
	 * Requerimiento buscar dato
	 * @param dato Dato a buscar
	 * @return dato encontrado
	 */
	public String buscar(String dato)
	{
		return datos.buscar(dato);
	}
	
	/**
	 * Requerimiento eliminar dato
	 * @param dato Dato a eliminar
	 * @return dato eliminado
	 */
	public String eliminar(String dato)
	{
		return datos.eliminar(dato);
	}
	
	
	public void m1()
	{
		
	}


}
